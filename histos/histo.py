import numpy as np
import matplotlib.pyplot as plt

import sys
#python histo.py dumpfile()
#To acquire dumpfile: 
#jellyfish histo ./experiment_results/output_92_S1_68_S1/normal.out > ./kmer-distribution-9_15_19/92_S1.distribution.txt

in_fil = sys.argv[1]

with open(in_fil) as fil:
    x = []
    height = []
    for line in fil:
        spl = line.split(" ")
        x.append(int(spl[0]))
        height.append(int(spl[1]))
    
    # print(x)
    # print(np.log(height))
    plt.bar(x, np.log(height),width=1)
    plt.title(in_fil)
    plt.xlabel("kmer count")
    plt.ylabel("log(frequency)")
    plt.savefig(f"{in_fil}.histo.png")
