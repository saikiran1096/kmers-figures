HeatMap :
To depict the relation between samples based on RMSD and JSD values.

Expected: 68_S1 vs 68_S2 will result with lower RMSD and JSD values compared to 68_S1 vs 104_s1
as the first senarios is self vs self comparsion of same patient Microbiome data obatained at different time points

Histo:
Histogram distrbution of k-mers with 10,000 bins for each samples. 
K-mer distrubtion plots could assist in justifying the filters values used to acuqiure the unique k-mers between samples.