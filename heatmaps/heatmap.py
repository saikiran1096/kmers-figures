import csv

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

#https://matplotlib.org/gallery/images_contours_and_fields/image_annotated_heatmap.html

in_fil = 'metrics.csv'
N = 6

rmsd_table = np.zeros((6,6))
js_table = np.zeros((6,6))

with open(in_fil) as fil:
    reader = csv.reader(fil)
    lookup = {}
    for row in reader:
        s1, s2, _, rmsd, js = row
        if s1 not in lookup:
            lookup[s1] = len(lookup)
            
        if s2 not in lookup:
            lookup[s2] = len(lookup)

        i = lookup[s1]
        j = lookup[s2]

        rmsd_table[i][j] = float(rmsd)
        js_table[i][j] = float(js)

        rmsd_table[j][i] = float(rmsd)
        js_table[j][i] = float(js)

fig, ax = plt.subplots()
im = ax.imshow(js_table)

ax.set_xticks(np.arange(len(lookup)))
ax.set_yticks(np.arange(len(lookup)))

ax.set_xticklabels(lookup.keys())
ax.set_yticklabels(lookup.keys())

cbar = ax.figure.colorbar(im, ax=ax)

cbar.ax.set_ylabel("JS value", rotation=-90, va="bottom")

plt.title("JS")
plt.show()


